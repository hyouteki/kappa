## Compiler optimizations
- [ ] Remove extra jump to final label instruction at the end of else block
- [ ] When pushing number to the stack; push the number directly instead of storing it inside the rcx and then pushing

## Compilable
- [x] Exit api
- [x] Variable Assignment Statement
- [ ] Print api
  - [x] just one string at a time
- [x] If Stmt
- [x] Top Level Function-Calls 
- [x] Integer Binary Expressions
- [x] Variable Mutation
- [x] While Statement
- [ ] Control Flow Statement
  - [ ] Validation 
  - [x] Break
  - [x] Continue
  - [ ] Return
- [ ] Function Defination
